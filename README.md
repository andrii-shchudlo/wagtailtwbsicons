# wagtailtwbsicons

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

A wagtail package to add [Twitter's Bootstrap Icons](https://github.com/twbs/icons) to administration icons list.

Currently using Bootstrap Icons v1.9.1

## Install

```
pip install wagtailtwbsicons
```

## Usage

- Add wagtailtwbsicons to INSTALLED_APPS in your project settings
```
INSTALLED_APPS = [
    ...
    'wagtailtwbsicons',
    ...     
]
```
- Have a look to [the available list of icons](https://icons.getbootstrap.com/#icons) and use the last class provided for web font in [the regular Wagtail way](https://docs.wagtail.org/en/latest/topics/streamfield.html#block-icons) (ex: for [person-square icon](https://icons.getbootstrap.com/icons/person-square/), the website provide to use "bi bi-person-square" class, so we need to use "bi-person-square")
```
class PersonBlock(blocks.StructBlock):
    first_name = blocks.CharBlock()
    ...
    class Meta:
        icon = 'bi-person-square'
```

### Using wagtailtwbsicons as an optionnal dependency
```
from django.apps import apps

class PersonBlock(blocks.StructBlock):
    first_name = blocks.CharBlock()
    ...
    class Meta:
        if apps.is_installed('wagtailtwbsicons'):
            icon = 'bi-person-square'
```
